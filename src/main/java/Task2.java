import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Deyneka Oleksandr
 * @version 1.0  28 Nov 2018
 * Implements pattern Command
 */

class Task2 {
    private Map<String, String> menu = new LinkedHashMap<>();
    private Map<String, Printable> methods = new LinkedHashMap<>();
    private static Scanner sc = new Scanner(System.in);
    String s;

    private Task2(String s) {
        this.s = s;
    }

    private String getS() {
        return "This is an object of command class: " + s;
    }

    void start() {
        initMaps();
        while (true) {
            System.out.println("Please, choose the option: ");
            printMenu();
            readInput();
        }
    }

    private void initMaps() {
        menu.put("L", " - Invoke command as lambda function");
        menu.put("M", " - Invoke command as method reference");
        menu.put("A", " - Invoke command as anonymous class");
        menu.put("O", " - Invoke command as object of command class");
        menu.put("E", " - Exit");

        methods.put("l", this::printLambda);
        methods.put("m", this::methodRef);
        methods.put("a", this::printAnonim);
        methods.put("o", this::printObject);
        methods.put("e", this::exit);
    }

    private void printMenu() {
        menu.forEach((n, m) -> System.out.println(n + m));
    }

    private void readInput() {
        String res = sc.nextLine().toLowerCase();
        while (!methods.containsKey(res)) {
            System.out.println("You entered the wrong command. Please, try again...");
            res = sc.nextLine();
        }

        System.out.println("Enter some argument: ");
        methods.get(res).print(sc.nextLine());
    }

    private void exit(String s) {
        System.exit(0);
    }

    private void printAnonim(String s) {

        Printable printSomething = new Printable() {
            @Override
            public void print(String s) {
                System.out.println("This is anonymous class: " + s);
            }
        };
        printSomething.print(s);
    }

    private void printLambda(String s) {

        Printable printLam = System.out::println;
        printLam.print("This is lambda method: " + s);
    }

    private void methodRef(String s) {
        System.out.println("This is method reference: " + s);
    }

    private void printObject(String s) {
        Task2 obj = new Task2(s);
        System.out.println(obj.getS());
    }

}
