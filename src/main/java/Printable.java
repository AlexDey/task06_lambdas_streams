/**
 * functional interface for Task2
 */
public interface Printable {
    void print(String s);
}
