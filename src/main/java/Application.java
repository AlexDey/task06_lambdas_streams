/**
 * @author Deyneka Oleksandr
 * @version 1.0  28 Nov 2018
 * Starts the tasks
 */
public class Application {
    /**
     * entry point
     *
     * @param args main arguments
     */
    public static void main(String[] args) {
//        new Task1().start();
//        new Task2().start();
//        new Task3(10).start();
        new Task4().start();
    }
}
