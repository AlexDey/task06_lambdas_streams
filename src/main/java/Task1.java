/**
 * @author Deyneka Oleksandr
 * @version 1.0 28 Nov 2018
 * Finds max and average values
 */
class Task1 {

    /**
     * method finds maximum value using functional interface
     */
    void findMax() {
        Countable max = (a, b, c) -> a > b && a > c ? (a) : (b > c ? b : c);
        System.out.println("The max value is: " + max.count(5, 8, 3));
    }

    /**
     * method finds average value using functional interface
     */
    void findAverage() {
        Countable average = (a, b, c) -> (b > a && c < a) || (b < a && c > a) ?
                (a) : ((a > b && c < b) || (a < b && c > b) ? b : c);
        System.out.println("The average value is: " + average.count(5, 8, 3));
    }

    public void start() {
        findMax();
        findAverage();
    }
}
