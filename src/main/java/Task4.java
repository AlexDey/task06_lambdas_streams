import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

/**
 * @author Deyneka Oleksandr
 * @version 1.0  28 Nov 2018
 * User enters some number of text lines (stop
 * reading text when user enters empty line).
 * Application returns:
 * - Number of unique words
 * - Sorted list of all unique words
 * - Occurrence number of each word in the text
 * - Occurrence number of each symbol except upper case characters
 */
class Task4 {
    private static Scanner scanner = new Scanner(System.in);
    private static List<String> list = new ArrayList<>();

    private static void readLines() {
        String line;
        while (!(line = scanner.nextLine()).isEmpty()) {
            list.add(line);
        }
    }

    void start() {
        readLines();
        System.out.println(countUniqueWords());
        viewSortedListOfUniqueWords();
        System.out.println(getOccurNumberOfEachWord());
        System.out.println(getCharOccurNumber());
    }

    private long countUniqueWords() {
        return list.stream().distinct().count();
    }

    private void viewSortedListOfUniqueWords() {
        List<String> sortedList = list.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        sortedList.forEach(System.out::println);
    }

    private Map<String, Long> getOccurNumberOfEachWord() {
        return list.stream()
                .collect(groupingBy(Function.identity(), counting()));
    }

    private int getCharOccurNumber() {
        return list.stream()
                .flatMap(e -> e.chars().boxed())
                .map(e -> (char) e.intValue())
                .filter(e -> !Character.isUpperCase(e))
                .collect(Collectors.toSet()).size();
    }
}
