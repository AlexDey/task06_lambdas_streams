import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author Deyneka Oleksandr
 * @version 1.0  28 Nov 2018
 * Task3 realization
 * Creates methods that returns list (or array) of random
 * integers. Methods use streams API and are
 * implemented using different Streams generators.
 * Counts average, min, max, sum of list values.
 * Counts number of values that are bigger than average.
 */
class Task3 {
    private int length;
    private Integer[] array;
    private List<Integer> list;

    Task3(int arrayLength) {
        this.length = arrayLength;
        array = new Integer[arrayLength];
    }

    void start() {
        randomArrayGenerator();
        randomListGenerator();
        countAverage();
        countMax();
        countMin();
        countSum();
        countNumbersBiggerThanAverage();
    }

    private void randomArrayGenerator() {
        array = Stream.generate(() -> (int) (Math.random() * 100))
                .limit(length)
                .toArray(Integer[]::new);
    }

    private void randomListGenerator() {
        list = Stream.generate(() -> (int) (Math.random() * 100))
                .limit(length)
                .collect(Collectors.toList());
    }

    private void countAverage() {
        IntSummaryStatistics iss = list.stream()
                .collect(Collectors.summarizingInt(n -> n));
        System.out.println("The average list value is: " + iss.getAverage());
    }

    private void countMin() {
        IntSummaryStatistics iss = list.stream()
                .collect(Collectors.summarizingInt(n -> n));
        System.out.println("The min list value is: " + iss.getMin());
    }

    private void countMax() {
        IntSummaryStatistics iss = list.stream()
                .collect(Collectors.summarizingInt(n -> n));
        System.out.println("The max list value is: " + iss.getMax());
    }

    private void countSum() {
        IntSummaryStatistics iss = list.stream()
                .collect(Collectors.summarizingInt(n -> n));
        System.out.println("The sum of list elements is: " + iss.getSum());
    }

    private void countNumbersBiggerThanAverage() {
        double average = list.stream()
                .collect(Collectors.summarizingInt(n -> n))
                .getAverage();
        long numbers = list.stream().filter(n -> n > average).count();
        System.out.println("Number of values that are bigger than average: " + numbers);
    }
}
